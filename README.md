# midic

A midi-to-cv module with both USB and DIN MIDI inputs. User defined presets map any MIDI message to any of 16 gate or CV outputs with 8-voice polyphony.

## Design

The brains of this module is a Raspberry Pi Pico with custom, open source, updatable firmware.

* 1.3 inch monochrome OLED display (SH1106) for the menu system
* Endless encoder with push-button to navigate the menus
* Two 5-pin MIDI DIN inputs
* Eight [MCP4921](https://www.taydaelectronics.com/mcp4921-e-sn-mcp4921-12-bit-dac-spi-interface-rail-to-rail-converter-ic.html) 12-bit DACs

It might use the Pico's Programmable IO (PIO) to offload the processing of incoming MIDI messages from the main CPU. Still considering how I might leverage the Pico's dual-core RP2040.
